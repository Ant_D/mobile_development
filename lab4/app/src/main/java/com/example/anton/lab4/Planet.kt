package com.example.anton.lab4

import android.graphics.Bitmap


data class Planet(val name: String, val image: Bitmap, val distToSun: String, val lifeIcon: Bitmap, val link: String)