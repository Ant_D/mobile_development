package com.example.anton.lab4

import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView


class PlanetAdapter(private val planets: Array<Planet>) : RecyclerView.Adapter<PlanetAdapter.ViewHolder>() {
    inner class ViewHolder(var planetView: View) : RecyclerView.ViewHolder(planetView) {
        val nameLabel = planetView.findViewById<TextView>(R.id.nameLabel)!!
        val imageView = planetView.findViewById<ImageView>(R.id.imageView)!!
        val distanceOutput = planetView.findViewById<TextView>(R.id.distanceOutput)!!
        val lifeIconView = planetView.findViewById<ImageView>(R.id.lifeIconView)!!
        val linkLabel = planetView.findViewById<TextView>(R.id.linkLabel)!!

        init {
            planetView.setOnClickListener {
                val intent = Intent(planetView.context, WebActivity::class.java).apply {
                    putExtra("link", linkLabel.text.toString())
                }
                planetView.context.startActivity(intent)
            }
        }
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val planet = planets[position]
        holder.nameLabel.text = planet.name
        holder.imageView.setImageBitmap(planet.image)
        holder.distanceOutput.text = planet.distToSun
        holder.lifeIconView.setImageBitmap(planet.lifeIcon)
        holder.linkLabel.text = planet.link
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.planet_layout, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return planets.size
    }
}