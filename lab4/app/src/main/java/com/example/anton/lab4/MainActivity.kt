package com.example.anton.lab4

import android.graphics.BitmapFactory
import android.media.Image
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.GridLayoutManager
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        recyclerView.layoutManager = GridLayoutManager(applicationContext, 2)

        val planets: Array<Planet> = arrayOf(
            Planet(resources.getString(R.string.mercury),
                    BitmapFactory.decodeResource(resources, R.drawable.mercury),
                    resources.getString(R.string.distance_from_mercury_to_sun),
                    BitmapFactory.decodeResource(resources, R.drawable.death_icon),
                    resources.getString(R.string.mercury_link)),
            Planet(resources.getString(R.string.venus),
                    BitmapFactory.decodeResource(resources, R.drawable.venus),
                    resources.getString(R.string.distance_from_venus_to_sun),
                    BitmapFactory.decodeResource(resources, R.drawable.death_icon),
                    resources.getString(R.string.venus_link)),
            Planet(resources.getString(R.string.earth),
                    BitmapFactory.decodeResource(resources, R.drawable.earth),
                    resources.getString(R.string.distance_from_earth_to_sun),
                    BitmapFactory.decodeResource(resources, R.drawable.life_icon),
                    resources.getString(R.string.earth_link)),
            Planet(resources.getString(R.string.mars),
                    BitmapFactory.decodeResource(resources, R.drawable.mars),
                    resources.getString(R.string.distance_from_mars_to_sun),
                    BitmapFactory.decodeResource(resources, R.drawable.death_icon),
                    resources.getString(R.string.mars_link)),
            Planet(resources.getString(R.string.jupiter),
                    BitmapFactory.decodeResource(resources, R.drawable.jupiter),
                    resources.getString(R.string.distance_from_jupiter_to_sun),
                    BitmapFactory.decodeResource(resources, R.drawable.death_icon),
                    resources.getString(R.string.jupiter_link)),
            Planet(resources.getString(R.string.saturn),
                    BitmapFactory.decodeResource(resources, R.drawable.saturn),
                    resources.getString(R.string.distance_from_saturn_to_sun),
                    BitmapFactory.decodeResource(resources, R.drawable.death_icon),
                    resources.getString(R.string.saturn_link)),
            Planet(resources.getString(R.string.uranus),
                    BitmapFactory.decodeResource(resources, R.drawable.uranus),
                    resources.getString(R.string.distance_from_uranus_to_sun),
                    BitmapFactory.decodeResource(resources, R.drawable.death_icon),
                    resources.getString(R.string.uranus_link)),
            Planet(resources.getString(R.string.neptune),
                    BitmapFactory.decodeResource(resources, R.drawable.neptune),
                    resources.getString(R.string.distance_from_neptune_to_sun),
                    BitmapFactory.decodeResource(resources, R.drawable.death_icon),
                    resources.getString(R.string.neptune_link))
        )

        recyclerView.adapter = PlanetAdapter(planets)
    }
}
