package com.example.anton.lab3

import android.content.Intent
import android.graphics.Bitmap
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.provider.MediaStore
import kotlinx.android.synthetic.main.activity_main.*
import android.support.design.widget.Snackbar

class MainActivity : AppCompatActivity() {

    private val TAKE_PICTURE_REQUEST = 1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btn_make_photo.setOnClickListener{
            if (!name_input.text.isEmpty()) {
                runCamera()
            }
            else {
                Snackbar.make(name_input, "Enter name before making photo", Snackbar.LENGTH_LONG).show()
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == TAKE_PICTURE_REQUEST && resultCode == RESULT_OK) {
            val imageBitmap = data?.extras?.get("data") as Bitmap
            val intent = Intent(this, ImageActivity::class.java).apply {
                putExtra("name", name_input.text.toString())
                putExtra("image", imageBitmap)
            }
            startActivity(intent)
        }
    }

    private fun runCamera() {
        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)  // say what we want to do
        if (intent.resolveActivity(packageManager) != null) {  // check if there is app which can help us
            startActivityForResult(intent, TAKE_PICTURE_REQUEST) // start camera
        }
    }
}
