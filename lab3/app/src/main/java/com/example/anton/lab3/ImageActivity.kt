package com.example.anton.lab3

import android.content.Intent
import android.graphics.Bitmap
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_image.*

class ImageActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_image)

        val name = intent.extras.get("name") as String
        val image = intent.extras.get("image") as Bitmap

        nameView.text = name
        imageView.setImageBitmap(image)
    }


}
