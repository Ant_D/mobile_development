package com.example.anton.lab2

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*
import android.support.design.widget.Snackbar

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        fun perform(op: (a: Double, b: Double) -> Double) {
            try {
                val a = number_input_1.text.toString().toDouble()
                val b = number_input_2.text.toString().toDouble()
                result_output.text = op(a, b).toString()
            }
            catch (e: Exception) {
                Snackbar.make(main_layout, "Enter parameters", Snackbar.LENGTH_LONG).show()
            }
        }

        btn_add.setOnClickListener {
            perform({a: Double, b: Double -> a + b})
        }

        btn_subtract.setOnClickListener {
            perform({a: Double, b: Double -> a - b})
        }

        btn_multiply.setOnClickListener {
            perform({a: Double, b: Double -> a * b})
        }

        btn_divide.setOnClickListener {
            perform({a: Double, b: Double -> a / b})
        }
    }
}
